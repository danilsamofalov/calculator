<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Калькулятор</title>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <script defer src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>
<body>
<div class="main">
    <div class="head">
        <h1>Калькулятор</h1>
        <form id="result" action="calculator/calc.php" method="POST">

            <span id="message"></span>

            <input type="text" name="num1" class="input-cont2" required placeholder="Введите число"
                   style="display: block">

            <select name="operator" class="select " required>
                <option value="+">+</option>
                <option value="-">-</option>
                <option value="*">*</option>
                <option value="/">/</option>
            </select>

            <input type="text" name="num2" class="input-cont" required placeholder="Введите число"
                   style="display: block">

            <div class="btn">
                <button type="button" name="send" onclick="result()">CALCULATE</button>
                <a href="calculator/connect.php">История</a>
            </div>

        </form>
    </div>

    <script type="text/javascript">

        function result() {
            var formData = $("#result").serialize();
            $.ajax({
                method: "POST",
                url: 'calculator/calc.php',
                data: formData,
                success: function (msg) {
                    console.log(msg);
                    $('#message').html(msg);
                }
            });
        }
    </script>
</div>
</form>
</body>
</html>







