<?php
//Тут поменяли хост на дефолтный для докера и системы -так работает при прямом включении из консоли
define('DB_HOST', 'mysql');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
//Добавила порт, чтобы точно подключение прощло
define('DB_PORT', 3306);
define('DB_NAME', 'calc');
//Тут передается название таблицы в которую будут писать версии миграций
define('DB_TABLE_VERSIONS', 'versions');


// Подключаемся к базе данных
function connectDB() {
    $errorMessage = 'Невозможно подключиться к серверу базы данных';
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME,DB_PORT);
        if (!$conn)
        throw new Exception($errorMessage);
    else {
        $query = $conn->query('set names utf8');
        if (!$query)
            throw new Exception($errorMessage);
        else
            return $conn;
    }
}

function connect(){
    $connect=mysqli_connect("mysql","root","root");
    if(!$connect)
    {
        die("СУБД не подключена".mysqli_errno());
    }
    mysqli_set_charset($connect,"utf8");
    if (!mysqli_select_db($connect,"calc"))
    {
        die("Ошибка БД".mysql_errno());
    }
    $GLOBALS['connect'] = $connect;
}