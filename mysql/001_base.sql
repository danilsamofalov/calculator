-- Таблица пользователей --
create table if not exists `calculator` (
                                            `calculator_id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                            `number1` varchar(255) NOT NULL,
                                            `operation` varchar(10) NOT NULL,
                                            `number2` varchar(255) NOT NULL,
                                            `calculate` varchar(255) NOT NULL
    )
    engine = innodb
    auto_increment = 1
    character set utf8
    collate utf8_general_ci;

-- Таблица versions --
create table if not exists `versions` (
    `id` int(10) unsigned not null auto_increment,
    `name` varchar(255) not null,
    `created` timestamp default current_timestamp,
    primary key (id)
    )
    engine = innodb
    auto_increment = 1
    character set utf8
    collate utf8_general_ci;
